[![GitLab pipeline status](https://gitlab.com/matthieu-bruneaux/ribbon/badges/master/pipeline.svg)](https://gitlab.com/matthieu-bruneaux/ribbon/commits/master)
[![Coverage report](https://gitlab.com/matthieu-bruneaux/ribbon/badges/master/coverage.svg)](https://matthieu-bruneaux.gitlab.io/ribbon/coverage/coverage.html)
[![R_CMD_CHECK](https://matthieu-bruneaux.gitlab.io/ribbon/R-CMD-check_badge.svg)](https://matthieu-bruneaux.gitlab.io/ribbon/R-CMD-check_output.txt)
[![Lifecycle Status](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/)

ribbon: an R package to easily draw ribbons in an R plot
========================================================

`ribbon` provides some simple functions to easily draw constant-width ribbons in R plots:

[![fig-01](img/fig-01.png)](img/fig-01.png)

The package is intentionally compact: it only provides a basic but versatile `ribbon()` function that adds ribbons to your base R plots, along with a few other low-level functions to provide fine-grained control when needed.

You can implement many different types of plots once you have the `ribbon()` function in your toolkit. See the [gallery](articles/tutorial-020-gallery.html) for detailled examples of what is possible.

## Installation

You can install the `ribbon` package from R with:

```
install.packages("devtools")
devtools::install_gitlab("matthieu-bruneaux/ribbon")
```

The package is not on CRAN at the moment, but hopefully it will be submitted once a stable first version is achieved.

## Documentation

Have a look at the [documentation](https://matthieu-bruneaux.gitlab.io/ribbon/) to learn how to use the package!

## Related resources

- alluvial and ggalluvial R packages https://cran.r-project.org/web/packages/ggalluvial/vignettes/ggalluvial.html
- Sankey diagrams https://www.r-graph-gallery.com/323-sankey-diagram-with-the-networkd3-library.html
- riverplot R package https://cran.r-project.org/web/packages/riverplot/
